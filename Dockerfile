
FROM rust:1.65.0
RUN rustup target add wasm32-unknown-unknown
RUN cargo install wasm-pack
